package org.grint.gamelive.controller;

import java.util.Random;

public class GameController {

    private static final GameController instance = new GameController();

    private int[][] field;

    private GameController() {}

    public int[][] generateField() {
        final Random generator = new Random();
        int mSize = 40;
        field = new int[mSize][mSize];

        for (int i = 0; i < mSize; i++) {
            for (int j = 0; j < mSize; j++) {
                field[i][j] = generator.nextInt(2);
            }
        }

        return field;
    }

    public int[][] calculateNextGeneration() {
        int[][] nextGeneration = new int[field.length][field.length];

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                int p = calculatePotential(i, j);
                nextGeneration[i][j] = checkCellState(p, field[i][j]);
            }
        }

        field = nextGeneration;
        return field;
    }

    private int calculatePotential(int i, int j) {
        int p = 0;
        for (int x = i - 1; x <= i + 1; x++) {
            for (int y = j - 1; y <= j + 1; y++) {
                if (x < 0 || y < 0 || x >= field.length || y >= field.length
                        || (x == i && y == j))
                    continue;
                if (field[x][y] > 0)
                    p++;
            }
        }
        return p;
    }

    private int checkCellState(int potential, int state) {
        if (potential == 3)
            return 1;
        else if (potential == 2)
            return state;
        else
            return 0;
    }

    public static GameController getInstance() {
        return instance;
    }

    public void setField(int[][] field) {
        this.field = field;
    }

    public int[][] getField() {
        return field;
    }
}