package org.grint.gamelive.ui;

import javax.swing.*;
import java.awt.*;


public class GameFieldPanel extends JPanel {

    private static final int CELL_SIZE = 10;

    private int[][] field;

    private Graphics2D graphics;

    public GameFieldPanel(int[][] field) {
        setField(field);
    }

    public void setField(int[][] field) {
        this.field = field;
    }

    public int[][] getField() {
        return field;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        graphics = (Graphics2D) g;

        // очищаем фон
        Rectangle r = getBounds();
        graphics.setBackground(Color.black);
        graphics.clearRect(0, 0, r.width, r.height);

        // очищаем фон
        Rectangle r2 = getBounds();

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                if (field[i][j] > 0) {
                    g.setColor(Color.green);
                    g.fillRect(i * (CELL_SIZE + 1), j * (CELL_SIZE + 1), CELL_SIZE, CELL_SIZE);
                }
            }
        }

    }

    public int getFieldSize() {
        int size = field.length * (CELL_SIZE + 1);
        return size;
    }
}