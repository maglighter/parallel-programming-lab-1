package org.grint.gamelive.ui;

import org.grint.gamelive.controller.GameController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {

    private static final boolean shouldFill = true;

    private static final boolean shouldWeightX = true;

    private static final boolean RIGHT_TO_LEFT = false;

    MainFrame() {
        JPanel mainPanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        mainPanel.setLayout(gbl);
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.WEST;
        c.fill = GridBagConstraints.BOTH;
        c.gridheight = 1;
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.gridx = GridBagConstraints.RELATIVE;
        c.gridy = GridBagConstraints.RELATIVE;
        c.insets = new Insets(0, 0, 0, 0);

        //создаем игровое поле
        final int[][] field = GameController.getInstance().generateField();
        final GameFieldPanel panel = new GameFieldPanel(field);

        //размеры ячейки для игрового поля
        c.ipady = panel.getFieldSize();
        c.ipadx = panel.getFieldSize();

        gbl.setConstraints(panel, c);
        mainPanel.add(panel);
        add(mainPanel);

        c.ipady = 0;
        c.ipadx = 0;
        c.gridwidth = 1;

        JButton generateButton = new JButton("Next Step");
        gbl.setConstraints(generateButton, c);
        mainPanel.add(generateButton);

        generateButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                int[][] nextGeneration = GameController.getInstance().calculateNextGeneration();
                panel.setField(nextGeneration);
                panel.repaint();
            }

        });

        JButton exitButton = new JButton("Exit");
        gbl.setConstraints(exitButton, c);
        mainPanel.add(exitButton);
        exitButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }

        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setTitle("Game \"Live\"");
        setVisible(true);
    }

    public static void main(String[] args) {
        MainFrame frame = new MainFrame();
    }
}


